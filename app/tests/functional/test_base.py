import pytest


def test_home(client):
    response = client.get("/")
    assert response.status_code == 200
    assert response.data == u"ಮಾನವನಾಗಿ ಹುಟ್ಟಿದ ಮೇಲೆ ಏನೇನ್ ಕಂಡಿ?".encode("utf-8")
