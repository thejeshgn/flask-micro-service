import pytest
from main.database import db
from application import create_app


@pytest.fixture(scope="module")
def client():

    # Flask provides a way to test your application by exposing the Werkzeug test Client
    # and handling the context locals for you.
    app, api, celery = create_app("testing")
    client = app.test_client()

    # Establish an application context before running the tests.
    ctx = app.app_context()
    ctx.push()

    yield client  # this is where the testing happens!

    ctx.pop()


@pytest.fixture(scope="module")
def init_database():
    # Create the database and the database table
    db.create_all()

    yield db  # this is where the testing happens!

    db.drop_all()
