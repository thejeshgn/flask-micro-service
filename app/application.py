import os
from flask import Flask, render_template
from flask_cors import CORS
from flask_restful import Resource, Api
from main import models
from config import getConfig
from main.database import db
from main import workers

app = None
api = None
celery = None
environment = None


def create_app(environment=None):
    if environment == None:
        if "FLASK_ENV" in os.environ:
            environment = os.environ["FLASK_ENV"]
        if environment is None or environment == "":
            print("setup FLASK_ENV")
    app = Flask(__name__, template_folder="templates")
    app.config.from_object(getConfig(environment))
    CORS(app)
    api = Api(app)
    db.init_app(app)

    # all the API end points setup
    from main.controller import framework_api

    api.add_resource(framework_api.FrameworkChangelog, "/api/changelog")
    api.add_resource(framework_api.FrameworkAsyncJobCall, "/api/job")

    app.app_context().push()

    # import controllers
    from main.controller import home

    app.add_url_rule("/", "index", home.index)
    app.app_context().push()

    celery = workers.celery
    workers.celery.conf.update(
        broker_url=app.config["CELERY_BROKER_URL"],
        backend=app.config["CELERY_RESULT_BACKEND"],
    )
    celery.Task = workers.ContextTask
    app.app_context().push()
    return app, api, celery


app, api, celery = create_app(None)

if __name__ == "__main__":
    app.run()
