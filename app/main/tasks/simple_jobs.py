from main.workers import celery


@celery.task()
def just_say_hello(name):
    print("INSIDE TASK")
    print("Hello {}".format(name))
