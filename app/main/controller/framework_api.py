from flask import Flask
from flask import current_app as app

from flask_restful import reqparse, abort, Api, Resource
from flask_restful import fields, marshal_with


from main.helpers.errorMessages import (
    validation_error,
    abort_if_doesnt_exist,
    not_supported,
)
from main.helpers.constants import pagination_fields
from main.helpers.constants import default_per_page, default_page

from main.database import db
from main import models
from main.controller import marshall_templates
from main.tasks import simple_jobs

parser = reqparse.RequestParser()
parser.add_argument("page")
parser.add_argument("per_page")


class FrameworkChangelog(Resource):
    @marshal_with(marshall_templates.framework_changelog_fields)
    def get(self):
        args = parser.parse_args()
        per_page = default_per_page
        page = default_page
        pagination = None
        if "per_page" in args and args["per_page"]:
            per_page = int(args["per_page"])

        if "page" in args and args["page"]:
            page = int(args["page"])

        pagination = models.FrameworkChangelog.query.filter().paginate(page, per_page)
        return {
            "changelog": pagination.items,
            "pagination": {
                "has_next": pagination.has_next,
                "has_prev": pagination.has_prev,
                "page": pagination.page,
                "per_page": pagination.per_page,
                "pages": pagination.pages,
                "total": pagination.total,
            },
        }


class FrameworkAsyncJobCall(Resource):
    @marshal_with(marshall_templates.framework_job_fields)
    def get(self):
        args = parser.parse_args()
        per_page = default_per_page
        page = default_page
        pagination = None
        if "per_page" in args and args["per_page"]:
            per_page = int(args["per_page"])

        if "page" in args and args["page"]:
            page = int(args["page"])

        pagination = models.FrameworkChangelog.query.filter().paginate(page, per_page)
        print("Calling job")
        job_id = simple_jobs.just_say_hello.delay("Thejesh GN")
        print(job_id)
        print("Not waiting for job to finish")
        return {"id": job_id, "name": "simple_jobs.just_say_hello"}
